###Regressão linear múltipla[1]###

A regressão linear múltipla tenta modelar a relação entre duas ou mais variáveis explicativas e uma variável resposta, ajustando uma equação linear aos dados observados. Cada valor da variável independente x está associado a um valor da variável dependente y.

###Métricas[2]###

	- Variância explicada
	- Erro absoluto médio
	- Erro quadrático médio
	- Erro logarítmico quadrático médio
	- Erro absoluto da mediana

###Referência###

[1]Multiple Linear Regression, Departament of Statistcs and Data Science, http://www.stat.yale.edu/Courses/1997-98/101/linmult.htm, Consultado 27 de Julho de 2018

[2]Regression-Metrics, Scikit-Learn, http://scikit-learn.org/stable/modules/model_evaluation.html#regression-metrics, Consultado 27 de Julho de 2018.