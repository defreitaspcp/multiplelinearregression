import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score, \
	explained_variance_score, mean_absolute_error, \
	mean_squared_log_error, median_absolute_error
from sklearn.model_selection import train_test_split


if __name__ == '__main__':
	columns = "age sex bmi bp s1 s2 s3 s4 s5 s6".split() # Declare the columns names
	diabetes = datasets.load_diabetes() # Call the diabetes dataset from sklearn
	df = pd.DataFrame(diabetes.data, columns=columns) # load the dataset as a pandas data frame
	#target is random number of 0-442
	y = diabetes.target # define the target variable (dependent variable) as y
	#splitting X and y into training and testing sets
	X_train, X_test, y_train, y_test = train_test_split(df, y, test_size=0.2)
	# Create linear regression object
	regr = linear_model.LinearRegression()
	# Train the model using the training sets
	regr.fit(X_train, y_train)
	# Make predictions using the testing set
	y_pred = regr.predict(X_test)
	# The coefficients
	print('Coefficients: \n', regr.coef_)
	#R^2 (coefficient of determination) regression score function.
	print("Variance score: %f"% r2_score(y_test, y_pred))
	#Median absolute error regression loss
	print("Median absolute error : %f"%median_absolute_error(y_test, y_pred))
	#Mean squared logarithmic error regression loss
	print("Mean squared log error : %f"%mean_squared_log_error(y_test, y_pred))
	#Explained variance regression score function
	print("Explained variance score : %f"%explained_variance_score(y_test, y_pred))
	#Mean absolute error regression loss
	print("Mean absolute error : %f"%mean_absolute_error(y_test, y_pred))
	#Mean squared error regression loss
	print("Mean squared error: %f"% mean_squared_error(y_test, y_pred))